
/**
 * Created by felix on 16.04.2015.
 */

var TeamspeakClient = require("node-teamspeak"),
    util = require("util"),
    conf = JSON.parse(require("fs").readFileSync("config.json"));

var client = new TeamspeakClient(conf.ServerIP);


client.send("login", {client_login_name: conf.ServerQueryUser, client_login_password: conf.ServerQueryPassword}, function (err, response){
    client.send("use", {sid: conf.ServerID}, function (err, response){
        var whoami = {};
        client.send("whoami", function (err, response) {
            whoami = response;
        });
        client.on("clientmoved", function (response) {
            if (typeof response !== 'undefined' && response.hasOwnProperty('clid')) {
                var clid = response.clid;
                if (response.ctid == conf.ListeningChannelID) {
                    client.send("clientinfo", { clid: clid }, function (err, response) {
                        if (typeof response !== 'undefined' && response.hasOwnProperty('client_nickname')) {
                            var clientDbId = response.client_database_id;
                            var clientName = response.client_nickname;
                            client.send("channelcreate", { channel_name: clientName + " - Channel", cpid: conf.BaseChannelID}, function (err, response) {
                                if (typeof response !== 'undefined' && response.hasOwnProperty('cid')) {
                                    var channelId = response.cid;
                                    client.send("clientmove", { clid: clid, cid: channelId }, function (err, response) {
                                        client.send("setclientchannelgroup", { cgid: conf.ChannelPermGrpID, cid: channelId, cldbid: clientDbId });
                                        client.send("clientmove", { clid: whoami.client_id, cid: conf.ListeningChannelID});
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
        client.send("servernotifyregister", { event: "channel", id: conf.ListeningChannelID});
    });
});

